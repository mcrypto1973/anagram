# ANAGRAM

## Observações

- Eu desenvolvi no Spring Tool Suite
- Estou executando com o comando
  - spring-boot:run -DskipTests -Dspring-boot.run.arguments=ARARA
- Os testes estão abrangendo somente o serviço que trata os anagramas, são apenas teste unitários
- Tudo foi enviado direto para o master, não utilizei o git flow nem me importei com issues (não é assim que trabalho, mas como era apenas um teste não levei em consideração este rito)

<span style="color:red">*Geralmente não se faz Java Docs sobre funções privadas ou protegidas, mas para um melhor entendimento do avaliador eu fiz*</span>

## Questão 1
<p>Duas palavras são chamadas ANAGRAMAS quando contém os mesmos caracteres na mesma frequencia de ocorrencias. Por exemplo, os anagramas de CAT são: CAT, ACT, TAC, ATC e CTA.</p>

<p>Escreva uma função que, dada uma String, gere como saída seus respectivos anagramas.</p>

Restrições:
- Considere A como a string de entrada e B como uma das strings de saida.
- 1 <= tamanho de A,B <= 50
- As strings A e B contém caracteres do alfabeto português brasileiro
- Calcule a quantidade maxima de combinações possíveis e informe na saida.
- Não imprimir mais do que 8 combinações possíveis e não menos que 5.
- Informe na saida caso não haja possibilidade de formar os anagramas.
  
Exemplo de saida:
- String de entrada: 
  - sabonete
- String de saida: 
  - abeenost, abeenots, abeensot, abeensto, abeentos, abeentso, 