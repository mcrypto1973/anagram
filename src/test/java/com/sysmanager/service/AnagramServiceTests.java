package com.sysmanager.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.sysmanager.exception.AnagramMinCombinationException;

/**
 * Testes do serviço que gera anagramas
 * 
 * @author Marcio Cruz de Almeida
 *
 */
@SpringBootTest
public class AnagramServiceTests {
	
	/**
	 * Este anagrama é inválido pois aesar do arranjo gerar 6 combinações 
	 * OVO OVO VOO VOO OOV OOV
	 * 123 321 213 231 132 312
	 * São gerados repetições, o código removerá as repetições e retornará
	 * menos que 5 anagramas, o que não é permitido
	 */
	private static final String ANAGRAM_INVALID = "OVO";
	
	
	private static final String PALINDROME = "ARARA";
	
	
	@Autowired
	private AnagramService anagramService;
	
	/**
	 * Ordena as letras de duas palavras e compara letra a letra para validar se possuem os mesmos caracteres
	 * 
	 * @param wordOne	primeira palavra
	 * @param wordTwo	segunda palavra
	 * @return 			<code>true</code> caso as duas palavras sejam anagramas<br>
	 * 					<code>false</code> caso as duas palavras não sejam anagramas
	 */
	private boolean areAnagram(char[] wordOne, char[] wordTwo) {
		
		if (wordOne.length != wordTwo.length) return false;
		
		Arrays.sort(wordOne);
		Arrays.sort(wordTwo);
		
		for (int i = 0; i < wordOne.length; i++) {
			if (wordOne[i] != wordTwo[i]) return false;
		}
		return true;
	}
	
	/**
	 * Testa se são gerados menos que 5 anagramas com a palavra fornecida
	 */
	@Test
	void anagramLessThanFiveCombinations() {
		try {
			anagramService.getAnagram(ANAGRAM_INVALID);
		} catch (AnagramMinCombinationException ex) {
			String expectedMessage = "Menos anagramas que o limite determinado!!!";
			String exceptionMsg = ex.getMessage();
			
			assertTrue(exceptionMsg.contains(expectedMessage));
		} 
	}
	
	/**
	 * Testa se são gerados mais que 5 anagramas e 8 ou menos anagramas com a palavra fornecida
	 * 
	 * @throws AnagramMinCombinationException
	 */
	@Test
	void anagramPalindrome() throws AnagramMinCombinationException {
		Set<String> list = anagramService.getAnagram(PALINDROME);
		
		assertFalse(list.size() < 5);
		assertFalse(list.size() > 8);
		
		for (String word : list) {
			assertTrue(areAnagram(PALINDROME.toCharArray(), word.toCharArray()));
		}	 
	}

}
