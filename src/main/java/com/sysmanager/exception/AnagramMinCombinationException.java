package com.sysmanager.exception;

/**
 * @author Marcio Cruz de Almeida
 *
 */
public class AnagramMinCombinationException extends Exception {

	private static final long serialVersionUID = -4363223496247510097L;
	
	public AnagramMinCombinationException(String errorMessage) {
		super(errorMessage);
	}

}
