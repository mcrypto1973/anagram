package com.sysmanager.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.sysmanager.exception.AnagramMinCombinationException;

/**
 * Serviço que cria anagramas a partir de uma palavra
 * 
 * @author Marcio Cruz de Almeida
 *
 */
@Service
public class AnagramService {
	
	private Set<String> anagramList;
	private static final int MAX_RESULT = 8;
	private static final int MIN_RESULT = 5;
	
	AnagramService() {
		anagramList = new HashSet<String>();  
	}
	
	/**
	 * Retorna o anagrama solicitado
	 * 
	 * @param word		palavra que origina o anagrama
	 * @return			lista de anagramas limitando ao máximo de 8 anagramas
	 * @throws 			AnagramMinCombinationException
	 */
	public Set<String> getAnagram(String word) throws AnagramMinCombinationException {
		
		Set<String> result = new HashSet<String>();

		if (word.length() == 0) return null;
		
		createAnagram(word, 0, word.length() - 1);
		
		anagramList.remove(word);
		
		if (anagramList.size() < MIN_RESULT) {
			throw new AnagramMinCombinationException("Menos anagramas que o limite determinado!!!");
		}
		
		if (anagramList.size() > MAX_RESULT) {
			int ind = 0;
			for (String anagram : anagramList) {
				ind++;
				result.add(anagram);
				if (ind == MAX_RESULT) 
					return result;
			}
		}
		else {
			result = anagramList;
		}
		
		return result;
	}
	
	/**
	 * Cria o anagrama da palavra informada e retorna uma lista de anagramas 
	 * 
	 * @param word		palavra que origina o anagrama
	 * @param start		posição do primeiro caracter
	 * @param end		posição do último caracter
	 * @return			lista de anagramas
	 * @throws			AnagramMinCombinationException 
	 */
	private void createAnagram(String word, int start, int end) throws AnagramMinCombinationException {
		
		if (start == end) {
			anagramList.add(word);
		}
		else {
			for (int i = start; i <= end; i++) {
				word = swap(word, start, i);
				createAnagram(word, start + 1, end);
				word = swap(word, start, i);
			}			
		}
	}
	
	/**
	 * Troca um caracter por outro de acordo com as posições passadas
	 * 
	 * @param word	palavra que deve ter seus caracteres trocados
	 * @param i		posição de um caracter da palavra
	 * @param j		posição de um caracter da palavra
	 * @return		nova palavra
	 */
	private String swap(String word, int i, int j) { 
        char temp; 
        char[] charArray = word.toCharArray(); 
        temp = charArray[i]; 
        charArray[i] = charArray[j]; 
        charArray[j] = temp; 
        return String.valueOf(charArray); 
    }
}
