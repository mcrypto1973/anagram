package com.sysmanager.job;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.sysmanager.exception.AnagramMinCombinationException;
import com.sysmanager.service.AnagramService;

@Component
public class AnagramJob implements ApplicationRunner {
	
	@Autowired
	private AnagramService anagramService;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {

		String word = "";
		for (String arg : args.getSourceArgs()) {
			word = arg;
		}

		try {
			System.out.println("String de entrada: " + word);
			Set<String> list = anagramService.getAnagram(word);
			System.out.println("String de saída: " + list);
		}
		catch (AnagramMinCombinationException ex) {
			System.out.println(ex.getMessage());
		}
	}

}
